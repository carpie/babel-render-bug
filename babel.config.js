module.exports = {
  presets: [ '@babel/preset-react' ],
  plugins: [ 'istanbul' ],
  auxiliaryCommentBefore: ' foobar '
}
